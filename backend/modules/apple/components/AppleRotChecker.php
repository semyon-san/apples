<?php


namespace backend\modules\apple\components;


use common\exceptions\ModelNotValidate;
use common\models\Apple;
use common\models\AppleStatus;

class AppleRotChecker
{
    const ROT_HOURS = 5;

    public static function rotApples(): void
    {
        /** @var Apple[] $apples */
        $apples = Apple::find()
            ->andWhere(['status_id' => AppleStatus::getIdByCode(AppleStatus::ON_GROUND)])
            ->notEaten()
            ->all();

        foreach ($apples as $apple) {
            $drop = new \DateTime($apple->date_drop);
            $now = new \DateTime();
            $interval = $now->diff($drop);
            if (
                $interval->y > 0
                || $interval->m > 0
                || $interval->d > 0
                || $interval->h >= self::ROT_HOURS
            ) {
                $apple->setStatus(AppleStatus::ROTTEN);
                if (!$apple->save()) {
                    throw new ModelNotValidate($apple);
                }
            }
        }
    }
}