<?php


namespace backend\modules\apple\components\commands;


use common\components\Utility;
use common\exceptions\ModelNotValidate;
use common\models\AppleStatus;

class AppleCommandRipOff extends AbstractAppleCommand
{
    public function execute(): void
    {
        $this->apple->status_id = AppleStatus::getIdByCode(AppleStatus::ON_GROUND);
        $this->apple->date_drop = Utility::getDateNow();

        if (!$this->apple->save()) {
            throw new ModelNotValidate($this->apple);
        }
    }
}