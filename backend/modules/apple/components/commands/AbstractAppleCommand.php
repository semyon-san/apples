<?php


namespace backend\modules\apple\components\commands;


use common\models\Apple;

abstract class AbstractAppleCommand implements AppleCommandInterface
{
    protected Apple $apple;

    public function __construct(Apple $apple)
    {
        $this->apple = $apple;
    }
}