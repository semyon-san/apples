<?php


namespace backend\modules\apple\components\commands;


use common\exceptions\ModelNotValidate;
use common\models\Apple;

class AppleCommandBite extends AbstractAppleCommand
{
    const DEFAULT_AMOUNT_BITE = 1;

    private int $amountToBite;

    public function __construct(Apple $apple, int $amountToBite=self::DEFAULT_AMOUNT_BITE)
    {
        $this->amountToBite = $amountToBite;

        parent::__construct($apple);
    }

    public function execute(): void
    {
        $this->apple->integrity -= $this->amountToBite;

        if (!$this->apple->save()) {
            throw new ModelNotValidate($this->apple);
        }
    }
}