<?php


namespace backend\modules\apple\components\commands;


interface AppleCommandInterface
{
    public function execute(): void;
}