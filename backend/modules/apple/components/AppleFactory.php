<?php

namespace backend\modules\apple\components;

use common\components\Utility;
use common\exceptions\ModelNotFoundException;
use common\exceptions\ModelNotValidate;
use common\models\Apple;
use common\models\AppleStatus;

final class AppleFactory
{
    /**
     * @param int $amount
     * @return Apple[]
     * @throws ModelNotValidate
     * @throws ModelNotFoundException
     */
    public static function generateApples(int $amount): array
    {
        $apples = [];

        for ($i = 0; $i < $amount; $i++) {
            $apple = new Apple([
                'status_id' => AppleStatus::getIdByCode(AppleStatus::ON_TREE),
                'integrity' => Apple::FULL_INTEGRITY,
            ]);

            $apple[] = $apple;

            if (!$apple->save()) {
                throw new ModelNotValidate($apple);
            }
        }

        return $apples;
    }

}