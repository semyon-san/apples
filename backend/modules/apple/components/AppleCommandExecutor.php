<?php


namespace backend\modules\apple\components;


use backend\modules\apple\components\commands\AppleCommandInterface;

class AppleCommandExecutor
{
    public static function executeCommand(AppleCommandInterface $command): void
    {
        $command->execute();
    }
}