<?php

namespace backend\modules\apple\controllers;

use backend\models\forms\AppleBiteForm;
use backend\models\forms\AppleGenerateForm;
use backend\modules\apple\components\AppleCommandExecutor;
use backend\modules\apple\components\commands\AppleCommandRipOff;
use common\models\Apple;
use common\models\AppleStatus;
use common\models\search\AppleSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;

class AppleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                    'bite' => ['GET', 'POST'],
                    'rip-off' => ['GET', 'POST']
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new AppleSearch();
        $dataProvider = $searchModel->searchExceptRotten(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGenerate()
    {
        $request = Yii::$app->request;

        $model = new AppleGenerateForm();

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Создаем яблоки",
                    'content' => $this->renderAjax('_create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#apple-grid-pjax',
                    'title' => "Создание",
                    'content' => '<span class="text-success">Получилось!</span>',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Создать еще', ['generate'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => "Создаем яблоки",
                    'content' => $this->renderAjax('_create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['index']);
            } else {
                return $this->render('_create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionBite($id)
    {
        $request = Yii::$app->request;
        $apple = $this->findModel($id);

        if($apple->status->code != AppleStatus::ON_GROUND) {
            throw new UnauthorizedHttpException('Нельзя кусать висящие на дереве или гнилые яблоки');
        }

        $model = new AppleBiteForm($apple);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Кусаем яблоко " . $id,
                    'content' => $this->renderAjax('_bite', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                $bittenText = $apple->integrity > 0
                    ? '<span class="text-success">Откушено, осталось ' . $apple->integrity . '%</span>'
                    : '<span class="text-success">Яблоко съедено. Убираем из списка.</span>';

                $biteMoreButton = $apple->integrity > 0
                    ? Html::a('Откусить еще', ['bite', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                    : '';

                return [
                    'forceReload' => '#apple-grid-pjax',
                    'title' => "Яблоко " . $id,
                    'content' => $bittenText,
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        $biteMoreButton
                ];
            } else {
                return [
                    'title' => "Кусаем яблоко " . $id,
                    'content' => $this->renderAjax('_bite', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['index']);
            } else {
                return $this->render('_bite', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionRipOff($id)
    {
        $apple = $this->findModel($id);

        if($apple->status->code != AppleStatus::ON_TREE) {
            throw new UnauthorizedHttpException('Яблоко не на дереве');
        }

        try {
            AppleCommandExecutor::executeCommand(new AppleCommandRipOff($apple));

            Yii::$app->session->setFlash('success', "Яблоко {$apple->id} сорвано.");

        } catch (\Exception $e) {
            Yii::$app->session->setFlash('danger', $e->getMessage());
        }

        return $this->redirect(['index']);
    }

    protected function findModel($id): Apple
    {
        if (($model = Apple::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Apple not found');
    }
}
