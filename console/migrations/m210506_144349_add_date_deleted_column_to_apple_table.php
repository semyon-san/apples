<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%apple}}`.
 */
class m210506_144349_add_date_deleted_column_to_apple_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('apple', 'date_deleted', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('apple', 'date_deleted');
    }
}
