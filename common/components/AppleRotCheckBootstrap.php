<?php
namespace common\components;

use backend\modules\apple\components\AppleRotChecker;
use yii\base\BootstrapInterface;

class AppleRotCheckBootstrap implements BootstrapInterface
{
    public function bootstrap($app)
    {
        AppleRotChecker::rotApples();
    }
}